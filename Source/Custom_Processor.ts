/**
 * @copyright 2019 Extend Applications Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {EventResponse} from './Request';

export let process940 = (dataBlob: EventResponse): EventResponse => {
    log.audit('process940 (custom) - dataBlob', dataBlob);
    dataBlob.meta.from = {
        idType: 'isaId',
        id: 'netsuitetest1'
    };

    dataBlob.meta.to = {
        idType: 'isaId',
        id: 'netsuitetest2'
    };
    return dataBlob;
};
